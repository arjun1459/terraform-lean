provider "aws" {
    # secret_key = "jv1ThhHQ62HrH17Gt/lKSKe/Y9Qm9Igme4zCK2wl"
    # access_key = "AKIATGH7JJGH4KW6U3R6"
    region = "ap-south-1"
}
variable "cidr-block-name" {
    description = " cidr block and name for resources "
    #default = "10.0.22.0/24"
    type = list(object({
        cidr_block =string 
        name =string
        }))
    }


resource "aws_vpc" "dev_vpc"{
    cidr_block = var.cidr-block-name[0].cidr_block  
    tags ={ 
        Name : var.cidr-block-name[0].name
    } 
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.dev_vpc.id
    cidr_block = var.cidr-block-name[1].cidr_block
    availability_zone = "ap-south-1b" 
    tags = {
        Name : var.cidr-block-name[1].name
    }
}

